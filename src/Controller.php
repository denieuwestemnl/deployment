<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment;

use Dns\Deployment\Controller\Deployment;
use Dns\Deployment\Helper\Log;

abstract class Controller
{
    /**
     * Includes the Log trait.
     */
    use Log {
        Log::__construct as private logConstructor;
    }

    /**
     * Contains the configuration.
     *
     * @var array
     */
    protected $config = [];

    /**
     * Contains the Deployment class.
     *
     * @var object
     */
    protected $deployment;

    /**
     * The object to connect to the API of the Git hosting service.
     *
     * @var \Dns\Deployment\Api
     */
    protected $git_api;

    /**
     * Sets the configuration.
     *
     * @param  array               $config
     * @param  \Dns\Deployment\Api $git_api
     * @throws \Exception
     */
    public function __construct($config, $git_api)
    {
        // set config
        $this->config  = $config;
        $this->git_api = $git_api;

        // call the constructor of the Log trait
        $this->logConstructor();

        // check for directory
        if (!isset($this->config['payloads-dir']) || empty($this->config['payloads-dir'])) {
            throw new \Exception('No payloads directory found');
        }

        // create new Deployment object
        $this->deployment = new Deployment($this->config, $git_api);
    }
}
