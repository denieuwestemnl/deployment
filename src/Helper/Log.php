<?php

/**
 * @author    Rolf den Hartog <development@rolfdenhartog.nl>
 * @copyright (c) 2015, Rolf den Hartog
 */

namespace Dns\Deployment\Helper;

use Monolog\Logger;

/**
 * Log trait. Can be used in every class.
 */
trait Log
{
    /**
     * Contains the Logger object.
     *
     * @var object
     */
    protected $log;

    /**
     * Contains the path to the log directory.
     *
     * @var string
     */
    protected $log_dir;

    /**
     * Contains the basename of the log file.
     *
     * @var string
     */
    protected $log_filename;

    /**
     * Contains the full path to the log file.
     *
     * @var string
     */
    protected $log_file;

    /**
     * Creates new Logger object.
     */
    public function __construct()
    {
        // create new logger
        $this->log = new Logger('deployment');

        // check for custom path
        if (!isset($this->config['log-dir'])) {
            return;
        }

        $this->log_dir      = $this->config['log-dir'];
        $this->log_filename = 'debug-' . date('Ymd') . '.log';
        $this->log_file     = $this->log_dir . '/' . $this->log_filename;

        // try to create log file
        if (!file_exists($this->log_file)) {
            @mkdir($this->log_dir);
            @touch($this->log_file);
        }

        // add handler
        if (file_exists($this->log_file)) {
            $this->log->pushHandler(new \Monolog\Handler\StreamHandler($this->log_file));
        }

        // delete old log files
        $this->deleteOldLogFiles();
    }

    /**
     * Deletes the logfiles older than 7 days.
     */
    public function deleteOldLogFiles()
    {
        // get all log files
        $files = glob($this->log_dir . '/debug-*.log');

        if (!is_array($files) || empty($files)) {
            return;
        }

        foreach ($files as $file) {
            // get date form filename
            $date = preg_replace('/debug-([0-9]{4})([0-9]{2})([0-9]{2}).log/', '$1-$2-$3', pathinfo($file, PATHINFO_BASENAME));

            // compare date
            if (strtotime($date) < strtotime('-7 days')) {
                // delete file
                unlink($file);
            }
        }
    }

    /**
     * Adds a debug message to the log.
     *
     * @param string $message
     */
    public function log($message)
    {
        $this->log->info($message);
    }
}
