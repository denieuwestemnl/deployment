<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment\Helper;

/**
 * Class File
 * @package Dns\Deployment\Helper
 */
class File
{
    /**
     * The base path. E.g. '/var/www/domain/public_html/'.
     *
     * @var string
     */
    private $base;

    /**
     * The full path of a file.
     *
     * @var string
     */
    private $path;

    /**
     * The contents of the file.
     *
     * @var string
     */
    private $contents = '';

    /**
     * Is utilized for reading data from inaccessible members.
     *
     * @param $property
     * @return mixed
     * @throws \Exception
     */
    function __get($property)
    {
        if (!property_exists($this, $property)) {
            throw new \Exception('Property \'' . $property . '\' doesn\'t exist');
        }

        return $this->$property;
    }

    /**
     * Runs when writing data to inaccessible members.
     *
     * @param $property
     * @param $value
     * @throws \Exception
     */
    function __set($property, $value)
    {
        if (!property_exists($this, $property)) {
            throw new \Exception('Property \'' . $property . '\' doesn\'t exist');
        }

        $this->$property = $value;
    }

    /**
     * - Checks the property.
     * - Checks the directory.
     * - Writes the contents to the file.
     * - Throws an exception of writing failed.
     *
     * @return bool
     * @throws \Exception
     */
    public function save()
    {
        $this->checkPath(false);

        // get directory of file
        $directory = dirname($this->path);
        // check for directory
        if (!file_exists($directory)) {
            $this->createDirectory($directory);
        }

        if (file_put_contents($this->path, $this->contents) === false) {
            throw new \Exception('Could not write contents to file');
        }

        return true;
    }

    /**
     * Deletes the file and its directories (if not empty).
     *
     * @return bool
     * @throws \Exception
     */
    public function delete()
    {
        $this->checkPath(true);

        if (file_exists($this->path) && !unlink($this->path)) {
            throw new \Exception('Could not delete file');
        }

        // delete directory
        $this->deleteDirectory(dirname($this->path));

        return true;
    }

    /**
     * - Calls the method to check the path.
     * - Sets the owner for a file.
     * - Throws an exception on failure or returns true.
     *
     * @param $owner
     * @return bool
     * @throws \Exception
     */
    public function changeOwner($owner)
    {
        $this->checkPath();

        if (!chown($this->path, $owner)) {
            throw new \Exception('Owner not changed');
        }

        return true;
    }

    /**
     * - Calls the method to check the path.
     * - Sets the mode for a file.
     * - Throws an exception on failure or returns true.
     *
     * @param  int $mode
     * @return bool
     * @throws \Exception
     */
    public function changeMode($mode = 0755)
    {
        $this->checkPath();

        if (!chmod($this->path, $mode)) {
            throw new \Exception('Mode not changed');
        }

        return true;
    }

    /**
     * - Checks if the property 'path' is set.
     * - Checks if the file exists.
     *
     * @param  bool $execute_file_exists Whether to check if the file exists. Default to true.
     * @throws \Exception
     */
    private function checkPath($execute_file_exists = true)
    {
        // check property
        if (!$this->path) {
            throw new \Exception('Path isn\'t set');
        }

        // check path
        if ($execute_file_exists && !file_exists($this->path)) {
            throw new \Exception('File doesn\'t exist');
        }
    }

    /**
     * Checks if directory exists and creates it if not.
     *
     * @param $directory
     * @throws \Exception
     */
    private function createDirectory($directory)
    {
        $directory_new = '';

        if ($this->base) {
            $directory = str_replace($this->base, '', $directory);
            $directory_new = $this->base;
        }

        // trim and explode
        $directory_exploded = explode('/', trim($directory, '/'));

        // loop directories
        foreach ($directory_exploded as $directory_part) {
            // add part to new directory
            $directory_new .= '/' . $directory_part;

            if (file_exists($directory_new)) {
                continue;
            }

            if (!mkdir($directory_new)) {
                throw new \Exception('Could not create directory \'' . $directory_new . '\'');
            }
        }
    }

    /**
     *
     *
     * @param $directory
     * @throws \Exception
     */
    private function deleteDirectory($directory)
    {
        if ($this->base) {
            $directory = str_replace($this->base, '', $directory);
        }

        // trim, explode and reverse order
        $directory_exploded = array_reverse(explode('/', trim($directory, '/')));

        // return on an empty array
        if (empty($directory_exploded)) {
            return;
        }

        // loop parts
        foreach ($directory_exploded as $directory_part) {
            // empty value of directory not empty
            if (empty($directory) || count(scandir($this->base . '/' . $directory)) > 2) {
                return;
            }

            // delete directory
            if (!rmdir($this->base . '/' . $directory)) {
                throw new \Exception('Could not delete directory \'' . $directory . '\'');
            }

            // set new directory
            $directory = str_replace('/' . $directory_part, '', $directory);
        }
    }
}
