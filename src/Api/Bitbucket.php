<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment\Api;

use Dns\Deployment\Api;
use GuzzleHttp\Client;

class Bitbucket extends Api
{
    /**
     * The account name for the repository. Can be a team name.
     *
     * @var string
     */
    private $account_name = '';

    /**
     * The slug of the repository.
     *
     * @var string
     */
    private $repository = '';

    /**
     * - Creates a new GuzzleHttp client.
     * - Sets the authentication for every request.
     *
     * @param string $username
     * @param string $password
     * @param string $account_name
     * @param string $repository
     */
    public function __construct($username, $password, $account_name, $repository)
    {
        // set client
        $this->client = new Client([
            'base_url' => 'https://api.bitbucket.org/1.0/',
            'defaults' => [
                'auth' => [
                    $username,
                    $password
                ]
            ]
        ]);

        $this->account_name = $account_name;
        $this->repository   = $repository;
    }

    /**
     * Returns the branch name.
     *
     * @param  string $payload
     * @return string
     * @throws \Exception
     */
    public function getBranch($payload)
    {
        $payload = json_decode($payload);

        // check for payload
        if (!$payload || !property_exists($payload, 'push')) {
            throw new \Exception('No (correct) payload found');
        }

        // 'new'
        if (isset($payload->push->changes[0]->new->name)) {
            $branch = $payload->push->changes[0]->new->name;
        }
        // 'forced'
        elseif (isset($payload->push->changes[0]->forced->name)) {
            $branch = $payload->push->changes[0]->forced->name;
        }
        // 'created'
        elseif (isset($payload->push->changes[0]->created->name)) {
            $branch = $payload->push->changes[0]->created->name;
        }
        // 'old'
        elseif (isset($payload->push->changes[0]->old->name)) {
            $branch = $payload->push->changes[0]->old->name;
        }

        // branch
        if (!isset($branch)) {
            throw new \Exception('Branch not found');
        }

        return $branch;
    }

    /**
     * Returns the commit hashes.
     *
     * @param  string $payload
     * @return array
     * @throws \Exception
     */
    public function getCommitHashes($payload)
    {
        $payload = json_decode($payload);

        // check for payload
        if (!$payload || !isset($payload->push->changes[0]->commits)) {
            throw new \Exception('No (correct) payload found');
        }

        // get commits
        $commits = $payload->push->changes[0]->commits;
        // start array
        $commit_hashes = [];

        // loop commits
        foreach ($commits as $commit) {
            $commit_hashes[] = $commit->hash;
        }

        return $commit_hashes;
    }

    /**
     * Returns an array with the changed and deleted files of a commit.
     *
     * @param  array $commit_hashes The commit hashes.
     * @return array
     * @throws \Exception
     */
    public function getFilesFromCommits($commit_hashes)
    {
        $files = [
            'place' => [],
            'delete' => [],
        ];

        // loop commit hashes
        foreach ($commit_hashes as $commit_hash) {
            // get file list from commit
            $commit_files = $this->getFilesFromCommit($commit_hash);

            $files['place']  = array_merge($files['place'],  $commit_files['place']);
            $files['delete'] = array_merge($files['delete'], $commit_files['delete']);
        }

        return $files;
    }

    /**
     * Returns an array with files of the commit.
     *
     * @param  string $commit_hash
     * @return array
     * @throws \Exception
     */
    public function getFilesFromCommit($commit_hash)
    {
        // create uri
        $uri = $this->createUri('repositories/{account_name}/{repository}/changesets/' . $commit_hash);
        // get commit
        $response = $this->client->get($uri);

        // check status
        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Invalid response from Bitbucket');
        }

        // get decoded body
        $commit = json_decode((string) $response->getBody());

        if (!isset($commit->files) || empty($commit->files)) {
            return [];
        }

        // start array
        $files = [
            'place' => [],
            'delete' => [],
        ];

        // loop files and place in correct array
        foreach ($commit->files as $file) {
            switch ($file->type) {
                case 'added':
                case 'modified':
                    $files['place'][$file->file] = $commit_hash;
                    break;
                case 'removed':
                    $files['delete'][$file->file] = $commit_hash;
                    break;
            }
        }

        return $files;
    }

    /**
     * Returns the raw contents of a file.
     *
     * @param  string $commit_hash The commit hash.
     * @param  string $file The path to the file in the repository.
     * @return string
     * @throws \Exception
     */
    public function getRawFile($commit_hash, $file)
    {
        // create uri
        $uri = $this->createUri('repositories/{account_name}/{repository}/raw/' . $commit_hash . '/' . $file);
        // get raw file
        $response = $this->client->get($uri);

        // check status
        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Invalid response from Bitbucket');
        }

        // return file
        return (string) $response->getBody();
    }

    /**
     * Replaces the account name and repo slug in the uri.
     *
     * @param  string $uri
     * @return string mixed
     */
    private function createUri($uri)
    {
        return str_replace([
            '{account_name}', '{repository}'
        ], [
            $this->account_name, $this->repository
        ], $uri);
    }
}
