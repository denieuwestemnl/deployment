<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment;

/**
 * Interface ApiInterface
 */
interface ApiInterface
{
    /**
     * - Creates a new GuzzleHttp client.
     * - Sets the authentication for every request.
     *
     * @param string $username
     * @param string $password
     * @param string $account_name
     * @param string $repository
     */
    public function __construct($username, $password, $account_name, $repository);

    /**
     * Returns the branch name.
     *
     * @param  string $payload
     * @return string
     * @throws \Exception
     */
    public function getBranch($payload);

    /**
     * Returns the commit hashes.
     *
     * @param  string $payload
     * @return array
     * @throws \Exception
     */
    public function getCommitHashes($payload);

    /**
     * Returns an array with the changed and deleted files of the commits.
     *
     * @param  array $commit_hashes The commit hashes.
     * @return array
     * @throws \Exception
     */
    public function getFilesFromCommits($commit_hashes);

    /**
     * Returns the files from a commit.
     *
     * @param  string $commit_hash
     * @return array
     */
    public function getFilesFromCommit($commit_hash);

    /**
     * @param  string $commit_hash The commit hash.
     * @param  string $file        The path to the file in the repository.
     * @return string
     * @throws \Exception
     */
    public function getRawFile($commit_hash, $file);
}
