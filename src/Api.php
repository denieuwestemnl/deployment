<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment;

/**
 * Abstract class Api
 */
abstract class Api implements ApiInterface
{
    /**
     * The http client to connect to the Git hosting service.
     *
     * @var \GuzzleHttp\Client
     */
    protected $client;
}
