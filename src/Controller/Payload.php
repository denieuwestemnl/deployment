<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment\Controller;

use Dns\Deployment\Controller;
use Dns\Deployment\Helper\File;

/**
 * Class Payload
 * @package Dns\Deployment
 */
class Payload extends Controller
{
    /**
     * Contains the json.
     *
     * @var string
     */
    private $json;

    /**
     * - Calls the parent constructor.
     * - Checks for the payloads directory.
     *
     * @param  array  $config
     * @param  object $git_api
     * @throws \Exception
     */
    public function __construct($config, $git_api)
    {
        parent::__construct($config, $git_api);

        // check IP address
        $this->checkIpAddress();

        // check for directory
        if (!isset($this->config['payloads-dir']) || empty($this->config['payloads-dir'])) {
            throw new \Exception('No payloads directory found');
        }
    }

    /**
     * - Checks the remote address with addresses from the config file.
     * - Returns if the IP address is in the range.
     * - Throws an exception if not.
     *
     * @return null
     * @throws \Exception
     */
    private function checkIpAddress()
    {
        // get remote address
        $remote_address = filter_input(INPUT_SERVER, 'REMOTE_ADDR');

        $this->log('Request from IP: ' . $remote_address);

        // empty config
        if (!isset($this->config['ip-addresses']) || empty($this->config['ip-addresses'])) {
            $this->log('IP addresses not set. You should!');
            return;
        }

        // loop IP addresses from config
        foreach ($this->config['ip-addresses'] as $ip_address_min => $ip_address_max) {
            if (
                ip2long($remote_address) >= ip2long($ip_address_min) &&
                ip2long($remote_address) <= ip2long($ip_address_max)
            ) {
                return;
            }
        }

        // no correct IP address found, throw exception
        throw new \Exception('Unknown IP address \'' . $remote_address . '\'');
    }

    /**
     * Sets the payload.
     *
     * @param string $json
     * @throws \Exception
     */
    public function setPayload($json)
    {
        // check payload
        if (empty($json)) {
            throw new \Exception('No payload found');
        }

        $this->log('Setting the payload');

        $this->json = $json;
    }

    /**
     * Saves the payload to a file.
     *
     * @param string $destination
     * @return bool
     * @throws \Exception
     */
    public function save($destination = '')
    {
        try {
            // read branch name
            $branch = $this->git_api->getBranch($this->json);
        } catch (\Exception $e) {
            throw $e;
        }

        // check if branch is in the array
        if (array_key_exists($branch, $this->config['destinations']) === false) {
            mail('rolfdns+webhook@gmail.com', 'Not saving payload (branch: ' . $branch . ')', $this->json);
            return false;
        }

        $path = $destination ? $destination : $this->config['payloads-dir'] . '/' . date('Ymd-His') . '.json';

        $this->log('Saving payload to \'' . $path . '\'');

        $file = new File();
        $file->path = $path;
        $file->contents = $this->json;
        return $file->save();
    }
}
