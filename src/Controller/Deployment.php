<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment\Controller;

use Dns\Deployment\Controller;
use Dns\Deployment\Helper\File;
use Dns\Deployment\Helper\Log;

/**
 * Class Deployment
 * @package Dns\Deployment\Controller
 */
class Deployment
{
    /**
     * Includes the Log trait.
     */
    use Log {
        Log::__construct as private logConstructor;
    }

    /**
     * The configuration array.
     *
     * @var array
     */
    private $config;

    /**
     * - Calls the parent constructor.
     * - Checks for temporary directory.
     *
     * @param  array               $config
     * @param  \Dns\Deployment\Api $git_api
     * @throws \Exception
     */
    public function __construct($config, $git_api)
    {
        $this->config  = $config;

        // call the constructor of the Log trait
        $this->logConstructor();

        // check for directory
        if (!isset($this->config['tmp-dir']) || empty($this->config['tmp-dir'])) {
            throw new \Exception('No tmp directory found');
        }
    }

    /**
     * - Checks for the payload file. Returns an empty string if not.
     * - Loads the payload and gets the branch.
     * - Checks if the payload is a 'destination' and returns the branch name or an empty string.
     *
     * @return string
     * @throws \Exception
     */
    public function beforeDeploy()
    {
        $this->log('Start deployment');

        // get payload file
        $payload_file = $this->getPayloadFile();

        if (empty($payload_file)) {
            return '';
        }

        // load branch
        $payload = $this->getPayload();
        // get branch name
        $branch  = $this->git_api->getBranch($payload);

        // check if branch is a 'destination'
        return isset($this->config['destinations'][$branch]) ? $branch : '';
    }

    /**
     * - Gets and loops the files list.
     * - Checks every file if it is downloaded. If not, it returns the file.
     * - Returns an empty message if all files are downloaded.
     *
     * @return string
     * @throws \Exception
     */
    public function downloadNextFile()
    {
        $files = $this->getFilesList();

        if (!isset($files->place) || empty($files->place)) {
            $this->log('No files to download');

            return '';
        }

        // loop files to place
        $i = 0;
        foreach ($files->place as $file => $commit_hash) {
            $tmp_file = $this->config['tmp-dir'] . '/' . 'place-' . $i . '-' . base64_encode($file);

            if (!file_exists($tmp_file)) {
                // save contents to temporary file
                file_put_contents($tmp_file, $this->git_api->getRawFile($commit_hash, $file));

                $this->log('Downloaded file \'' . $file . '\'');

                return $file;
            }

            $i++;
        }

        $this->log('No files left to download');

        return '';
    }

    /**
     * - Globs the files starting with 'place-' and checks if the result is an array and not empty.
     * - Gets the payload to get the branch name for the destination.
     * - Loops the files.
     * - Gets the path by decoding the filename.
     * - Saved the contents to the file or throws an exception if failed.
     * - Deletes the tmp file and returns the path to the file.
     *
     * @return string
     * @throws \Exception
     */
    public function placeNextFile()
    {
        $tmp_files = glob($this->config['tmp-dir'] . '/place-*');

        // loop files
        if (!is_array($tmp_files) || empty($tmp_files)) {
            $this->log('No files to place');

            return '';
        }

        // get payload
        $payload = $this->getPayload();
        // get branch
        $branch = $this->git_api->getBranch($payload);

        // loop files
        foreach ($tmp_files as $tmp_file) {
            // get the encoded filename
            $file_base64_encoded = preg_replace('/.*\/place-[0-9]+-(.*)/', '$1', $tmp_file);
            // decode
            $file_base64_decoded = base64_decode($file_base64_encoded);

            // set base directory
            $base     = $this->config['destinations'][$branch];
            // complete path
            $path     = $base . '/' . $file_base64_decoded;
            // get contents of tmp file
            $contents = file_get_contents($tmp_file);

            $file = new File();
            $file->base = $base;
            $file->path = $path;
            $file->contents = $contents;

            // place file
            if ($file->save()) {
                // delete tmp file
                unlink($tmp_file);

                $this->log('Placing file \'' . $file_base64_decoded . '\'');

                // return filename
                return $file_base64_decoded;
            } else {
                throw new \Exception('File \'' . $file_base64_decoded . '\' not placed');
            }
        }

        $this->log('No files left to place');

        return '';
    }

    /**
     * - Loads the payload to get the branch.
     * - Gets the files list and checks for the delete property.
     * - Loops the files.
     * - Checks if the file exists and deletes the file.
     * - Returns the filename if the file is deleted. Thrown an exception if not.
     * -
     *
     * @return string
     * @throws \Exception
     */
    public function deleteNextFile()
    {
        // get payload
        $payload = $this->getPayload();
        // get branch
        $branch = $this->git_api->getBranch($payload);
        // get files
        $files = $this->getFilesList();

        if (!isset($files->delete) || empty($files->delete)) {
            $this->log('No files to delete');

            return '';
        }

        // loop files to delete
        foreach ($files->delete as $file_to_delete => $commit_hash) {
            // base path
            $base = $this->config['destinations'][$branch];
            // complete path
            $path = $base . '/' . $file_to_delete;

            if (!file_exists($path)) {
                continue;
            }

            $file = new File();
            $file->base = $base;
            $file->path = $path;

            if ($file->delete()) {
                $this->log('Deleting file \'' . $file_to_delete . '\'');

                return $file_to_delete;
            } else {
                throw new \Exception('Could not delete file \'' . $file_to_delete . '\'');
            }
        }

        $this->log('No files files left to delete');

        return '';
    }

    /**
     * Deletes the files list and the payload file.
     *
     * @return bool
     */
    public function afterDeploy()
    {
        $this->log('Finishing deployment');

        return unlink($this->config['tmp-dir'] . '/files.json') && unlink($this->getPayloadFile());
    }

    /**
     * @throws \Exception
     */
    private function getPayload()
    {
        // get filename
        $payload_file = $this->getPayloadFile();
        // read file
        $payload = file_get_contents($payload_file);

        // throw an exception if reading failed
        if (!$payload) {
            throw new \Exception('Could not read payload file \'' . $payload_file . '\'');
        }

        // decode and return
        return $payload;
    }

    /**
     * Returns the oldest payload available.
     *
     * @return string
     */
    private function getPayloadFile()
    {
        $files = glob($this->config['payloads-dir'] . '/*.json');

        // check result of glob
        if (!is_array($files) || empty($files)) {
            return '';
        }

        $payload_file = '';

        // loop files
        foreach ($files as $file) {
            // get date from current file
            $date_payload_file = pathinfo($payload_file, PATHINFO_BASENAME);
            // get date from filename
            $date = pathinfo($file, PATHINFO_BASENAME);

            if (empty($payload_file) || $date < $date_payload_file) {
                $payload_file = $file;
            }
        }

        return $payload_file;
    }

    /**
     * - Sets tha path to the json file.
     * - Gets the payload from the webhook.
     * - Gets the commit hash from the payload.
     * - Gets the files list based on the commit hash.
     * - Saves the files list as json and throws an exception if failed.
     * - Returns the decoded file with the files list.
     *
     * @return object
     * @throws \Exception
     */
    private function getFilesList()
    {
        $path = $this->config['tmp-dir'] . '/files.json';

        if (!file_exists($path)) {
            $payload       = $this->getPayload();
            $branch        = $this->git_api->getBranch($payload);
            $commit_hashes = $this->git_api->getCommitHashes($payload);
            $files         = isset($this->config['destinations'][$branch]) ? $this->git_api->getFilesFromCommits($commit_hashes) : [];

            if (!file_put_contents($path, json_encode($files))) {
                throw new \Exception('Could not save contents to file \'files.json\'');
            }
        }

        return json_decode(file_get_contents($path));
    }
}
