<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment\Controller;

use Dns\Deployment\Helper\File;

/**
 * Class Cron
 * @package Dns\Deployment\Controller
 */
class Cron extends Controller
{
    /**
     * The name of the branch.
     *
     * @var string
     */
    private $branch;

    /**
     * The SHA1 commit hash.
     *
     * @var string
     */
    private $commit_hash;

    /**
     * The array with the files that needs to be changed.
     *
     * @var array
     */
    private $files_to_place = [];

    /**
     * Counter.
     *
     * @var integer
     */
    private $files_placed = 0;

    /**
     * The array with the files that needs to be deleted.
     *
     * @var array
     */
    private $files_to_delete = [];

    /**
     * Counter.
     *
     * @var integer
     */
    private $files_deleted = 0;

    /**
     * - Encodes and sets the payload json from the webhook.
     * - Checks the IP addressed.
     * - Checks the config.
     * - Calls the methods to deploy the files.
     *
     * @throws \Exception
     */
    public function run()
    {
        $this->getFirstPayload();
        $this->readPayload();
        $this->readCommit();
        $this->placeFiles();
        $this->deleteFiles();
    }

    /**
     * - Creates the url for getting the commit.
     * - Makes the http call.
     * - Checks the status code and throws an exception if it's not 200.
     * - Returns the body of the call.
     *
     * @return string
     * @throws \Exception
     */
    public function readCommit()
    {
        $this->log('Read commit');

        // create uri
        $uri = $this->createUri('repositories/{account_name}/{repo_slug}/changesets/' . $this->commit_hash);
        // get commit
        $response = $this->client->get($uri);

        // check status
        if ($response->getStatusCode() !== 200) {
            throw new \Exception('Invalid response from Bitbucket');
        }

        // get body decoded
        $commit = json_decode((string) $response->getBody());

        if (!isset($commit->files) || empty($commit->files)) {
            return;
        }

        // loop files and place in correct array
        foreach ($commit->files as $file) {
            switch ($file->type) {
                case 'added':
                case 'modified':
                    $this->files_to_place[] = $file->file;
                    break;
                case 'removed':
                    $this->files_to_delete[] = $file->file;
                    break;
            }
        }
    }

    /**
     * Loops the files and calls the method to delete a file. A counter is
     * updated on success.
     */
    public function placeFiles()
    {
        $this->log('Place files');

        foreach ($this->files_to_place as $file) {
            if ($this->placeFile($file)) {
                $this->files_placed++;
            }
        }

        $this->log($this->files_placed . ' files placed.');
    }

    /**
     * - Gets the full path to the file.
     * - Puts the content in the file.
     * - Returns a boolean.
     *
     * @param string $file
     * @return boolean
     */
    public function placeFile($file)
    {
        $path = $this->getFullPathToFile($file);

        $file = new File();
        $file->path = $path;
        $file->contents = $this->getRawFile($file);

        return $file->save();
    }
}
