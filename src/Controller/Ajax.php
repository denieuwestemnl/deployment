<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

namespace Dns\Deployment\Controller;

use Dns\Deployment\Controller;

/**
 * Class Ajax
 */
class Ajax extends Controller
{
    /**
     * - Creates a method name for the action.
     * - Checks if the method exists and throws an Exception if not.
     * - Calls the method and returns its result json encoded.
     *
     * @param  string $action
     * @return string
     * @throws \Exception
     */
    public function doAction($action)
    {
        $method = $this->createMethodName($action);

        if (!method_exists($this, $method)) {
            throw new \Exception('Method \'' . $method . '\' doesn\'t exist.');
        }

        echo json_encode($this->$method());
    }

    /**
     * Returns a camelCased string.
     *
     * @param  string $string
     * @param  string $replace Defaults to '-'.
     * @return string
     */
    private function createMethodName($string, $replace = '-')
    {
        return lcfirst(
            str_replace(
                ' ',
                '',
                ucwords(
                    str_replace(
                        $replace,
                        ' ',
                        $string
                    )
                )
            )
        );
    }

    /**
     * Returns the result of the downloadFile method.
     *
     * @return array
     */
    private function startDeploying()
    {
        $branch = $this->deployment->beforeDeploy();

        if (!$branch) {
            return [
                'log' => 'Nothing to deploy',
            ];
        }

        return [
            'action' => 'download-file',
            'log'    => 'Start deploying (branch: ' . $branch . ')',
        ];
    }

    /**
     * Calls the method to download the next file. The method returns the path to the file. If the path is empty, it
     * means that all the files are downloaded or there were no files to download. In that case the method returns an
     * array with the action for placing files.
     *
     * @return array
     */
    private function downloadFile()
    {
        // download next file
        $downloaded_file = $this->deployment->downloadNextFile();

        // nothing left to download
        if ($downloaded_file === '') {
            return [
                'action' => 'place-file',
                'log'    => 'Downloaded all the files or no files to download',
            ];
        }

        // download next file and return action and log
        return [
            'action' => 'download-file',
            'log'    => 'Downloaded file \'' . $downloaded_file . '\'',
        ];
    }

    /**
     * Calls the method to place the next file. The method returns the path of the file. If the path is empty, it means
     * that all the files are placed. In that case, the method returns an array with an action to delete files.
     *
     * @return array
     * @throws \Exception
     */
    private function placeFile()
    {
        $placed_file = $this->deployment->placeNextFile();

        // nothing left to place
        if ($placed_file === '') {
            return [
                'action' => 'delete-file',
                'log'    => 'Placed all the files or no files to place',
            ];
        }

        // place next file and return action and log
        return [
            'action' => 'place-file',
            'log'    => 'Placed file \'' . $placed_file . '\'',
        ];
    }

    /**
     * Calls the method to delete the next file.
     *
     * @return array
     * @throws \Exception
     */
    private function deleteFile()
    {
        $deleted_file = $this->deployment->deleteNextFile();

        // nothing left to delete
        if ($deleted_file === '') {
            return [
                'action' => 'finish-deployment',
                'log'    => 'Deleted all the files or no files to delete',
            ];
        }

        // delete next file and return action and log
        return [
            'action' => 'delete-file',
            'log'    => 'Deleted file \'' . $deleted_file . '\'',
        ];
    }

    /**
     * Calls the method to delete the files list.
     *
     * @return array
     */
    private function finishDeployment()
    {
        $this->deployment->afterDeploy();

        return [
            'log' => 'Deployment finished'
        ];
    }
}
