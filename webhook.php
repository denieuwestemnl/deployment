<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

// include autoloaders
require_once 'autoload.php';
require_once 'vendor/autoload.php';

// get configuration
$config = require_once 'config.php';
// set git api
$git_api = new \Dns\Deployment\Api\Bitbucket(
    $config['credentials']['username'],
    $config['credentials']['password'],
    $config['credentials']['account'],
    $config['credentials']['repository']
);

// save payload
try {
    $payload = new \Dns\Deployment\Controller\Payload($config, $git_api);
    $payload->setJson(file_get_contents('php://input'));
    $payload->save();
} catch(\Exception $e) {
    if (file_exists($config['log-dir'])) {
        file_put_contents(
            $config['log-dir'] . '/error.log',
            $e->getFile() . '#' . $e->getLine() . ' ' . $e->getMessage()
        );
    }
}
