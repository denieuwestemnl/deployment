/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

var Deployment = function(){
    /**
     * Calls the method to start deploying.
     */
    this.construct = function()
    {
        var that = this;

        $('#btn-start-deployment').on('click', function(){
            // empty log list
            $('ul').empty();
            // call method
            that.doAction('start-deploying');
        });
    };

    /**
     * Makes the request to execute an action on the server.
     *
     * @param {String} action
     */
    this.doAction = function(action)
    {
        var that = this;

        $.ajax({
            data: {action: action},
            url: 'ajax.php'
        }).done(function(json){
            // decode json
            var data = JSON.parse(json);

            // set log
            that.log(data.log);

            if (data.action) {
                setTimeout(function(){
                    that.doAction(data.action);
                }, 150);
            }
        }).fail(function(){
            that.log('Action failed. Check the inspector.');
        });
    };

    /**
     * Sets a log message.
     *
     * @param message
     */
    this.log = function(message)
    {
        var date = new Date();
        var message_date;

        message_date  = date.getFullYear();
        message_date += '-' + (date.getMonth() < 10 ? '0' + date.getMonth() : date.getMonth());
        message_date += '-' + (date.getDate() < 10 ? '0' + date.getDate() : date.getDate());
        message_date += ' ' + (date.getHours() < 10 ? '0' + date.getHours() : date.getHours());
        message_date += ':' + (date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes());
        message_date += ':' + (date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds());

        $('ul').append('<li><pre>[' + message_date + '] ' + message + '</pre></li>');

        // animate to bottom
        $('html, body').animate({
            scrollTop: $('ul').offset().top + $('ul').height()
        }, 150);
    };

    this.construct();
}();
