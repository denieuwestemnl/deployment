<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

?>
<!DOCTYPE html>
<html>
    <head>
        <link href="/assets/styles/bootstrap.min.css" rel="stylesheet">
        <title>Deployment</title>
    </head>
    <body>
        <div class="container">
            <h1>
                Deployment
                <button class="btn btn-primary pull-right" id="btn-start-deployment" type="button">Start deploying</button>
            </h1>
            <ul class="list-unstyled">
            </ul>
        </div>
        <script src="/assets/scripts/jquery-2.1.4.min.js"></script>
        <script src="/assets/scripts/deployment.js"></script>
    </body>
</html>
