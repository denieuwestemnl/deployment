<?php

/**
 * Contains the loader for the classes.
 *
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015, denieuwestem.nl
 */

spl_autoload_register(
    /**
     * Autoloader for the theme classes.
     *
     * @param string $class
     */
    function($class)
    {
        // ignore if not in 'Dns\Deployment' namespace
        if (strpos($class, 'Dns\\Deployment') === false) {
            return;
        }

        // create path
        $path  = __DIR__;
        $path .= '/src';
        $path .= str_replace(
            ['Dns\\Deployment', '\\'],
            ['',            '/'],
            $class
        );
        $path .= '.php';

        // check if file exists
        if (!file_exists($path)) {
            trigger_error('File \'' . $path . '\' not found for class \'' . $class . '\'', E_USER_ERROR);
        }

        require_once $path;

        // check if class exists
        if (!class_exists($class) && !interface_exists($class) && !trait_exists($class)) {
            trigger_error('Class \'' . $class . '\' not found', E_USER_ERROR);
        }
    }
);
