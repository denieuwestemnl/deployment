<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

// include autoloader
require_once 'autoload.php';
require_once 'vendor/autoload.php';

// get config
$config = require_once '../config.php';
// get action
$action = filter_input(INPUT_GET, 'action');

// create new Api object
$git_api = new \Dns\Deployment\Api\Bitbucket(
    $config['credentials']['username'],
    $config['credentials']['password'],
    $config['credentials']['account'],
    $config['credentials']['repository']
);

// create new Ajax object
$ajax = new \Dns\Deployment\Controller\Ajax($config, $git_api);
$ajax->doAction($action);
