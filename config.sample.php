<?php

/**
 * @author    Rolf den Hartog <rolf@denieuwestem.nl>
 * @copyright (c) 2015 denieuwestem.nl
 */

return [
    /**
     * Bitbucket credentials.
     */
    'credentials' => [
        'username' => 'your_username',
        'password' => 'yourp@ssword',
        'account'  => 'account_name_or_team',
        'repo'     => 'repo-slug',
    ],

    /**
     * The branches and its destinations.
     */
    'destinations' => [
        'develop' => '/path/to/develop/site',
        'master'  => '/path/to/master/site',
    ],

    /**
     * Path to the temporary directory for the files that are going to be deployed.
     */
    'tmp-dir' => '/path/to/tmp',

    /**
     * Path to the logs directory.
     */
    'log-dir' => '/path/to/log',

    /**
     * White list of IP addresses. If not used, no IP address is blocked (not recommended).
     */
    'ip-addresses' => [
        // Bitbucket IP addresses
        // 131.103.20.160/27
        '131.103.20.161' => '131.103.20.190',
        // 165.254.145.0/26
        '165.254.145.1'  => '165.254.145.62',
        // 104.192.143.0/24
        '104.192.143.1'  => '104.192.143.254',

        '192.168.10.1'
    ],
];
